package com.company;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.*;


public class Main {

    public static final int MATCH_ID = 0;
    public static final int INNING = 1;
    public static final int BATTING_TEAM = 2;
    public static final int BOWLING_TEAM = 3;
    public static final int OVER = 4;
    public static final int BALL = 5;
    public static final int BATSMAN = 6;
    public static final int NON_STRIKER = 7;
    public static final int BOWLER = 8;
    public static final int IS_SUPER_OVER = 9;
    public static final int WIDE_RUNS = 10;
    public static final int BYE_RUNS = 11;
    public static final int LEG_BYE_RUNS = 12;
    public static final int NO_BALL_RUNS = 13;
    public static final int PENALTY_RUNS = 14;
    public static final int BATSMAN_RUNS = 15;
    public static final int EXTRA_RUNS = 16;
    public static final int TOTAL_RUNS = 17;
    public static final int ID = 0;
    public static final int YEAR = 1;
    public static final int CITY = 2;
    public static final int DATE = 3;
    public static final int TEAM1 = 4;
    public static final int TEAM2 = 5;
    public static final int TOSS_WINNER = 6;
    public static final int TOSS_DECISION = 7;
    public static final int RESULT = 8;
    public static final int DL_APPLIED = 9;
    public static final int WINNER = 10;
    public static final int WIN_BY_RUNS = 11;
    public static final int WIN_BY_WICKETS = 12;
    public static final int PLAYER_OF_MATCH = 13;
    public static final int VENUE = 14;
    public static final int UMPIRE1 = 15;
    public static final int UMPIRE2 = 16;

    public static void main(String[] args) {
        List<Match> matches = getMatches();
        List<Delivery> deliveries = getDeliveries();
        findNumberOfMatchesPlayerPerYear(matches);
        findNumberOfMatchesWonByTeam(matches);
        findExtraRunsConcededPerTeamInYear(deliveries, matches, 2016);
        findTopEconomicalBowlerOfYear(deliveries, matches, 2015);
        findBestPlayer(matches);
    }

    private static void findNumberOfMatchesPlayerPerYear(List<Match> matches) {
        System.out.println("Number of Matches Per Year");
        Map<Integer, Integer> numberOfMatchesPlayerPerYear = new HashMap<>();
        for (Match match : matches) {
            if (numberOfMatchesPlayerPerYear.containsKey(match.getYear())) {
                numberOfMatchesPlayerPerYear.put(match.getYear(), (numberOfMatchesPlayerPerYear.get(match.getYear()) + 1));
            } else {
                numberOfMatchesPlayerPerYear.put(match.getYear(), 1);
            }
        }

        for (Map.Entry<Integer, Integer> entry : numberOfMatchesPlayerPerYear.entrySet()) {
            System.out.println(entry.getKey() + " : " + entry.getValue());
        }
        System.out.println();
    }

    private static void findNumberOfMatchesWonByTeam(List<Match> matches) {
        System.out.println("Number of Matches wom by a Team");
        Map<String, Integer> numberOfMatchesWonByTeam = new HashMap<>();
        for (Match match : matches) {
            if (numberOfMatchesWonByTeam.containsKey(match.getWinner()))
                numberOfMatchesWonByTeam.put(match.getWinner(), numberOfMatchesWonByTeam.get(match.getWinner()) + 1);
            else
                numberOfMatchesWonByTeam.put(match.getWinner(), 1);
        }

        for (Map.Entry<String, Integer> entry : numberOfMatchesWonByTeam.entrySet()) {
            if (!entry.getKey().equals(""))
                System.out.println(entry.getKey() + " : " + entry.getValue());
        }
        System.out.println();
    }

    private static void findExtraRunsConcededPerTeamInYear(List<Delivery> deliveries, List<Match> matches, int year) {
        System.out.println("Extra Runs Conceded per team in 2016");
        Map<String, Integer> extraRunsConcededPerTeam = new HashMap<>();
        int firstMatchId = findFirstMatchId(matches, year);
        int lastMatchId = findLastMatchId(matches, year);
        for (Delivery delivery : deliveries) {
            if (delivery.getMatchId() >= firstMatchId && delivery.getMatchId() <= lastMatchId) {
                if (extraRunsConcededPerTeam.containsKey(delivery.getBowlingTeam())) {
                    extraRunsConcededPerTeam.put(delivery.getBowlingTeam(), extraRunsConcededPerTeam.get(delivery.getBowlingTeam()) + delivery.getExtraRuns());
                } else {
                    extraRunsConcededPerTeam.put(delivery.getBowlingTeam(), delivery.getExtraRuns());

                }
            }
        }

        for (Map.Entry<String, Integer> entry : extraRunsConcededPerTeam.entrySet()) {
            System.out.println(entry.getKey() + " : " + entry.getValue());
        }
        System.out.println();
    }

    private static void findTopEconomicalBowlerOfYear(List<Delivery> deliveries, List<Match> matches, int year) {
        Map<String, Integer> runsConcededByBowler = new HashMap<>();
        Map<String, Integer> ballsBowledByBowler = new HashMap<>();
        int firstMatchId = findFirstMatchId(matches, year);
        int lastMatchId = findLastMatchId(matches, year);
        double topEconomy = 100.0;
        String playerWithTopEconomy = "";

        for (Delivery delivery : deliveries) {
            if (delivery.getMatchId() <= lastMatchId && delivery.getMatchId() >= firstMatchId)
                if (runsConcededByBowler.containsKey(delivery.getBowler())) {
                    runsConcededByBowler.put(delivery.getBowler(), runsConcededByBowler.get(delivery.getBowler()) + (delivery.getTotalRuns() - delivery.getByeRuns() - delivery.getLegByeRuns()));
                    ballsBowledByBowler.put(delivery.getBowler(), ballsBowledByBowler.get(delivery.getBowler()) + 1);
                } else {
                    runsConcededByBowler.put(delivery.getBowler(), (delivery.getTotalRuns() - delivery.getLegByeRuns() - delivery.getByeRuns()));
                    ballsBowledByBowler.put(delivery.getBowler(), 1);
                }
        }

        for (Map.Entry<String, Integer> entry : runsConcededByBowler.entrySet()) {
            double oversBowledByCurrentBowler = (double) ballsBowledByBowler.get(entry.getKey()) / 6;
            double economyOfCurrentBowler = ((double) runsConcededByBowler.get(entry.getKey())) / oversBowledByCurrentBowler;
            if (economyOfCurrentBowler < topEconomy) {
                topEconomy = economyOfCurrentBowler;
                playerWithTopEconomy = entry.getKey();
            }
        }
        System.out.printf("Most Economical Bowler of 2015: " + playerWithTopEconomy + " with Economy %.2f\n", topEconomy);
    }

    private static void findBestPlayer(List<Match> matches) {
        System.out.println();
        Map<String, Integer> numberOfAwards = new HashMap<>();
        int highestNumberOfAwards = 0;
        String bestPlayer = "";

        for (Match match : matches) {
            if (numberOfAwards.containsKey(match.getPlayerOfMatch())) {
                numberOfAwards.put(match.getPlayerOfMatch(), numberOfAwards.get(match.getPlayerOfMatch()) + 1);
            } else {
                numberOfAwards.put(match.getPlayerOfMatch(), 1);
            }
        }

        for (Map.Entry<String, Integer> entry : numberOfAwards.entrySet()) {
            if (entry.getValue() > highestNumberOfAwards) {
                highestNumberOfAwards = entry.getValue();
                bestPlayer = entry.getKey();
            }
        }
        System.out.println("Best Player: " + bestPlayer + " with " + highestNumberOfAwards + " awards as player of match");
    }

    private static List<Match> getMatches() {
        List<Match> deliveriesList = new ArrayList<>();
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader("matches.csv"));
            String line = bufferedReader.readLine();
            line = bufferedReader.readLine();
            while (line != null) {
                String[] attributeValues = line.split(",");
                Match match = new Match();
                match.setId(Integer.parseInt(attributeValues[ID]));
                match.setYear(Integer.parseInt(attributeValues[YEAR]));
                match.setCity(attributeValues[CITY]);
                match.setDate(attributeValues[DATE]);
                match.setTeam1(attributeValues[TEAM1]);
                match.setTeam2(attributeValues[TEAM2]);
                match.setTossWinner(attributeValues[TOSS_WINNER]);
                match.setTossDecision(attributeValues[TOSS_DECISION]);
                match.setResult(attributeValues[RESULT]);
                match.setDlApplied(Integer.parseInt(attributeValues[DL_APPLIED]));
                match.setWinner(attributeValues[WINNER]);
                match.setWinByRuns(Integer.parseInt(attributeValues[WIN_BY_RUNS]));
                match.setWinByWickets(Integer.parseInt(attributeValues[WIN_BY_WICKETS]));
                match.setPlayerOfMatch(attributeValues[PLAYER_OF_MATCH]);
                match.setVenue(attributeValues[VENUE]);
                match.setUmpire1(attributeValues[UMPIRE1]);
                match.setUmpire2(attributeValues[UMPIRE2]);
                deliveriesList.add(match);
                line = bufferedReader.readLine();
            }
        } catch (Exception e) {
            System.out.println("Exception: " + e);
        }
        return deliveriesList;
    }

    private static List<Delivery> getDeliveries() {
        List<Delivery> deliveries = new ArrayList<>();
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader("deliveries.csv"));
            String line = bufferedReader.readLine();
            line = bufferedReader.readLine();
            while (line != null) {
                String[] attributes = line.split(",");
                Delivery delivery = new Delivery();
                delivery.setMatchId(Integer.parseInt(attributes[MATCH_ID]));
                delivery.setInning(Integer.parseInt(attributes[INNING]));
                delivery.setBattingTeam(attributes[BATTING_TEAM]);
                delivery.setBowlingTeam(attributes[BOWLING_TEAM]);
                delivery.setOver(Integer.parseInt(attributes[OVER]));
                delivery.setBall(Integer.parseInt(attributes[BALL]));
                delivery.setBatsman(attributes[BATSMAN]);
                delivery.setNonStriker(attributes[NON_STRIKER]);
                delivery.setBowler(attributes[BOWLER]);
                delivery.setIsSuperOver(Integer.parseInt(attributes[IS_SUPER_OVER]));
                delivery.setWideRuns(Integer.parseInt(attributes[WIDE_RUNS]));
                delivery.setByeRuns(Integer.parseInt(attributes[BYE_RUNS]));
                delivery.setLegByeRuns(Integer.parseInt(attributes[LEG_BYE_RUNS]));
                delivery.setNoBallRuns(Integer.parseInt(attributes[NO_BALL_RUNS]));
                delivery.setPenaltyRuns(Integer.parseInt(attributes[PENALTY_RUNS]));
                delivery.setBatsmanRuns(Integer.parseInt(attributes[BATSMAN_RUNS]));
                delivery.setExtraRuns(Integer.parseInt(attributes[EXTRA_RUNS]));
                delivery.setTotalRuns(Integer.parseInt(attributes[TOTAL_RUNS]));
                deliveries.add(delivery);
                line = bufferedReader.readLine();
            }
        } catch (Exception e) {
            System.out.println("Exception: " + e);
        }
        return deliveries;
    }

    private static int findFirstMatchId(List<Match> matches, int year) {
        int firstMatchId = 0;
        for (Match match : matches) {
            if (match.getYear() == year) {
                firstMatchId = match.getId();
                break;
            }
        }
        return firstMatchId;
    }

    private static int findLastMatchId(List<Match> matches, int year) {
        int lastMatchId = 0;
        for (Match match : matches) {
            if (match.getYear() == year) {
                lastMatchId = match.getId();
            }
        }
        return lastMatchId;
    }
}